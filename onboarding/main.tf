terraform {
  cloud {
    organization = "axians-labs"

    workspaces {
      tags = ["onboarding"]
    }
  }
}

provider "vsphere" {
  user           = var.vsphere_user
  password       = var.vsphere_password
  vsphere_server = var.vsphere_server

  allow_unverified_ssl = true
}

data "vsphere_datacenter" "dc" {
  name = var.vsphere_conf.datacenter
}

data "vsphere_datastore" "datastore" {
  name          = var.vsphere_conf.datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_compute_cluster" "cluster" {
  name          = var.vsphere_conf.cluster
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network_server" {
  name          = var.vm_mgmt_network
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_virtual_machine" "template" {
  name          = var.vm_template
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_virtual_machine" "vm_instance" {
  for_each = {
    for name, machine in var.vms :
    name => machine
  }

  name             = each.key
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id
  folder           = var.vm_folder

  num_cpus         = var.vm_cpus
  memory           = var.vm_memory
  guest_id         = data.vsphere_virtual_machine.template.guest_id

  scsi_type        = data.vsphere_virtual_machine.template.scsi_type

  network_interface {
    network_id   = data.vsphere_network.network_server.id
    adapter_type = data.vsphere_virtual_machine.template.network_interface_types[0]
  }

  disk {
    label            = "disk0"
    size             = var.vm_disk_size
    eagerly_scrub    = data.vsphere_virtual_machine.template.disks.0.eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.template.disks.0.thin_provisioned
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
  }

  cdrom {
    client_device = true
  }

  vapp {
    properties = {
      "user-data" = base64encode(templatefile("templates/cloud-init.tmpl", { ip = each.value.ip,
                                                                  gw = var.gateway,
                                                                  dns = var.dns_primary,
                                                                  ssh_public_keys = var.ssh_public_keys}))
    }
  }
}

#
# Generate ansible inventory
#

data "template_file" "inventory" {
  template = file("templates/inventory.tpl")

  vars = {
    group_name = var.ansible_inventory_group
    connection_strings_hosts = join("\n", formatlist("%s ansible_user=%s ansible_host=%s",
      keys(var.vms),
      var.user_name,
      [for instance in vsphere_virtual_machine.vm_instance :
        instance.default_ip_address ]))
  }
}

resource "null_resource" "inventories" {
  provisioner "local-exec" {
    command = "echo '${data.template_file.inventory.rendered}' > ${var.inventory_file}"
  }

  triggers = {
    template = data.template_file.inventory.rendered
  }
}
