variable "vsphere_server" {
    description = "vsphere server for the environment"
}

variable "vsphere_user" {
    description = "vsphere server for the environment"
}

variable "vsphere_password" {
    description = "vsphere server password for the environment"
    sensitive = true
}

variable "vsphere_conf" {
  type = object({
    datacenter = string
    datastore = string
    cluster = string
    })
}

variable "vm_template" {
    description = "VM template to use for VM deployment"
}

variable "vm_mgmt_network" {
    description = "Port group for VM management"
}

variable "vms" {
  description = "Virtual machines"
  type = map(object({
    node_type = string
    ip        = string
  }))
}

variable "ansible_inventory_group" {}

variable "user_name" {}

variable "vm_folder" {
    description = "Folder to store this VM in"
}

variable "vm_cpus" {}
variable "vm_memory" {}
variable "vm_disk_size" {}

variable "gateway" {}
variable "dns_primary" {
  default = "8.8.4.4"
}
variable "dns_secondary" {
  default = "8.8.8.8"
}
variable "ssh_public_keys" {
  type = list(string)
}

variable "inventory_file" {
  default = "inventory.ini"
}
