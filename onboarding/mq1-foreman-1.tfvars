vsphere_server = "mq-vcenter.axians.lab"
vsphere_conf = {
  datacenter = "Hyperflex-vcenter"
  datastore = "Datastore_SAS_INN_1"
  cluster = "Hyperflex-Cluster"
}
vm_template = "Debian 11 - vApp cloud-init (OVF)"
vm_mgmt_network = "1051 - MGMT Lab"
user_name = "debian"
vm_folder = "/INN-CEN/diego.nuevo"
vm_cpus = 2
vm_memory = 16384
vm_disk_size = 64
vms = {
  "mq1-foreman-1" : {
    "node_type" : "foreman",
    "ip" : "192.168.104.46/24"
  }
}
ansible_inventory_group = "all"
gateway = "192.168.104.1"
dns_primary = "192.168.104.4"
ssh_public_keys = [
  # Put your public SSH key here
  "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDxgljIdy3u0XwZvGqzD0uxs6ESvtEi8/WBpUDj+xAu+rfxdSsWT8/zd8b49VEbHbxip5//KUjbPdIdKNk+ZJdVOaEl/JG2oWRiDeDAvF9xtZ8w6o7qWh5pNf9OdST92D8OVQk4Y4rLMxfase+hl2+9RrPcOo/fsNfHxWarxtIzNxfbBSNxoFKg0cwz6riNS1OYgPBahx+AJuqti9lWyZDO2A5uCBn/3MDwSsSf7HdwobLPnHpnoqoEahmoL89KsRD9AtyUFFTRzMDEsQvBu0Zm/XNHNrgxGJArq4pxavBhxtmckQUeeljSoaOJLxlYupV1+W/zzfBY4aiZyXooglLHBw25PNMUtw3CNUTt8S35hA/tL/5bdrblO7nca6RBSMrPyiRnQqWJ0UIMAAocWjrX/fGArUxqqpVZeuT8dLKEXV6rm+rdg41OEgh0JKSh6htnA23zu/93MUGVtSgMztcvnEzzmJp/ulWrpL8hs9jP2JnV5Har/ckFZWojNHs4B1s= diego@Decrux"
]
inventory_file = "mq1-foreman-1.ini"
