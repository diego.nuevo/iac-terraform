vsphere_server = "changeme"
#vsphere_user = "changeme"
#vsphere_password = "changeme"
vsphere_conf = {
  datacenter = "changeme"
  datastore = "changeme"
  cluster = "changeme"
}
vm_template = "Debian 10 - vApp cloud-init"
vm_mgmt_network = "1051 - MGMT Lab"
user_name = "debian"
vm_folder = "changeme"
vm_cpus = 2
vm_memory = 16384
vm_disk_size = 64
vms = {
  "host" : {
    "node_type" : "this field is not used",
    "ip" : "192.168.104.x/24"
  }
}
gateway = "192.168.104.1"
dns_primary = "192.168.104.4"
ssh_public_keys = [
  # Put your public SSH key here
  "ssh-rsa ... user@host"
]
