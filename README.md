# Introduction

Use the code in this repository to deploy and configure new VMs on VMware infrastructure.

You will need to perform the following tasks:
- Deploy your VM(s)
- Configure your VM(s) by running the relevant Ansible playbooks

# Deployment (onboarding)

In order to deploy a new VM, do the following from the `onboarding` directory:

Initialize your terraform directory (this is a one-time task). The main.cf file is configured to save Terraform state on Terraform Cloud, under the "axians-labs" organization (change it accordingly to suit your environment):
```
terraform init
```

Create a new terraform workspace for this deployment:
```
terraform workspace new containerlab
```

Deploy the new VM(s):
```
terraform plan -var-file mq1-clab-1.tfvars
terraform apply -var-file mq1-clab-1.tfvars
```
NOTE: The first step (plan) is optional

# Configuration

Run the Ansible playbook to configure and customize the VM, and install the desired software:
```
ansible-playbook -i onboarding/mq1-clab-1.ini -e "ansible_user=debian" ansible-playbooks/deploy-containerlab.yml -e clear_fingerprints=true
```
NOTE: The `-e clear_fingerprints=true` option sets a variable to ignore and accept the SSH fingerprint (useful for newly onboarded VMs, as is the case).