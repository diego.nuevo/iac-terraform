# Instructions

1. Run the deploy-common.yml playbook with the debian user (the user is preinstalled on the VM template)
```
ansible-playbook -i onboarding/mq1-clab-1.ini -e "ansible_user=debian" ansible-playbooks/deploy-common.yml -e clear_fingerprints=true
```

2. Run the deploy-\*.yml playbook that suits your needs (for example deploy-siab.yml or deploy-containerlab.yml) with the user of your choice (ensure the user is added to the list in the group_vars config before running step 1)
```
ansible-playbook -i onboarding/mq1-clab-1.ini -e "ansible_user=axians" ansible-playbooks/deploy-containerlab.yml
ansible-playbook -i onboarding/mq1-foreman-1.ini -e "ansible_user=axians" ansible-playbooks/deploy-foreman.yml
ansible-playbook -i onboarding/mq1-seba-box-1.ini -e "ansible_user=axians" ansible-playbooks/deploy-siab.yml
```
